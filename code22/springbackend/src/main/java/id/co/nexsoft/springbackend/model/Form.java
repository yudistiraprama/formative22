package id.co.nexsoft.springbackend.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Form {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    @Lob
    private String boxOne;
    @Lob
    private String boxTwo;
    @Lob
    private String boxThree;
    @Lob
    private String quote;

    public Form() {
    }

    public Form(int id, String boxOne, String boxTwo, String boxThree, String quote) {
        this.id = id;
        this.boxOne = boxOne;
        this.boxTwo = boxTwo;
        this.boxThree = boxThree;
        this.quote = quote;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBoxOne() {
        return boxOne;
    }

    public void setBoxOne(String boxOne) {
        this.boxOne = boxOne;
    }

    public String getBoxTwo() {
        return boxTwo;
    }

    public void setBoxTwo(String boxTwo) {
        this.boxTwo = boxTwo;
    }

    public String getBoxThree() {
        return boxThree;
    }

    public void setBoxThree(String boxThree) {
        this.boxThree = boxThree;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }
}
