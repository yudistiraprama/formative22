package id.co.nexsoft.springbackend.service;

import id.co.nexsoft.springbackend.model.Form;
import id.co.nexsoft.springbackend.repository.FormRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FormService {

    @Autowired
    FormRepository formRepo;

    public Form getForm(int id) {
        return formRepo.findById(id);
    }

    public Form saveForm(Form form) {
        return formRepo.save(form);
    }

    public Form saveForm(int id, Form form) {
        form.setId(id);
        return formRepo.save(form);
    }

}
