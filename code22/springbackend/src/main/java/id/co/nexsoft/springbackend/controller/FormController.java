package id.co.nexsoft.springbackend.controller;

import id.co.nexsoft.springbackend.model.Form;
import id.co.nexsoft.springbackend.service.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class FormController {

    @Autowired
    FormService formService;

    @GetMapping(path = "/get/{id}")
    public Form getData(@PathVariable(value = "id") int id) {
        return formService.getForm(id);
    }

    @PutMapping(path = "/addBox/{id}")
    public Form saveData(@RequestBody Form form,
                                   @PathVariable int id) {
        return formService.saveForm(id, form);
    }

}
