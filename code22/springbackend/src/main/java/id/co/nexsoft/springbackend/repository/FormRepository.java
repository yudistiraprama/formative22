package id.co.nexsoft.springbackend.repository;

import id.co.nexsoft.springbackend.model.Form;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FormRepository extends CrudRepository<Form, Integer> {

    Form findById(int id);
    List<Form> findAll();

}
