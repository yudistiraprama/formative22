import './App.css';
import Form from './component/FormInput';
import Home from './component/Home';
import About from './component/About';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/form" component={Form} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
