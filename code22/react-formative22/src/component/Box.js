import React, { Component } from 'react';
import axios from 'axios';

class Box extends Component {

  constructor(props) {
    super(props);
    this.state = {
      box: ''
    };
  }

  componentDidMount() {
    axios.get('http://localhost:8080/get/1')
      .then(res => {
        this.setState({
          box: res.data
        });
      })
  }

  render() {
    return (
      <div>
        <div className="four columns">
          <h5>Photography</h5>
          <p>{this.state.box.boxOne}</p>
          <p>
            <a href="/" className="readmore">Learn more</a>
          </p>
        </div>
        <div className="four columns">
          <h5>Artwork</h5>
          <p>{this.state.box.boxTwo}</p>
          <p>
            <a href="/" className="readmore">Learn more</a>
          </p>
        </div>
        <div className="four columns">
          <h5>Logos</h5>
          <p>{this.state.box.boxThree}</p>
          <p>
            <a href="/" className="readmore">Learn more</a>
          </p>
        </div>
      </div>
    )
  }
}

export default Box;