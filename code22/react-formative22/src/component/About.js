import React, { Component } from "react";
import Header from './Header';
import Footer from './Footer';
import ContentAbout from './ContentAbout';

class About extends Component {

  render() {
    return (
      <div>
        <Header />
        <div id="subheader">
          <div class="row">
            <div class="twelve columns">
              <p class="left">ABOUT US</p>
              <p class="right">Meet Our Team</p>
            </div>
          </div>
        </div>

        <div class="row">
          <ContentAbout />
          <div class="four columns">
            <div class="teamwrap">
              <img src="/im/team1.jpg" width="300" height="200" alt="" />
            </div>
            <div class="teamwrap">
              <img src="/im/team2.jpg" width="300" height="200" alt="" />
            </div>
            <div class="teamwrap">
              <img src="/im/team3.jpg" width="300" height="200" alt="" />
            </div>
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}

export default About;