import React, { Component, Fragment } from 'react';
import Box from './Box';
import Testimonial from './Testimonial';
import Header from './Header';
import Footer from './Footer';
import ImageSlider from './ImageSlider';
import SubHeader from './SubHeader';
import Card from './Card';

class Home extends Component {

  render() {
    return (
      <Fragment>
        <Header />
        <ImageSlider />
        <SubHeader />
        <Card />

        <div className="row">
          <div className="centersectiontitle">
            <h4>What we do</h4>
          </div>
          <Box />
        </div>

        <div className="row">
          <div id="testimonials">
            <Testimonial />
          </div>
        </div>
        <Footer />
        </Fragment>

    )
  }
}

export default Home;

