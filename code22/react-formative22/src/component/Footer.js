import React, { Component } from 'react';

class Footer extends Component {

  render() {
    return (
      <div>
        <div id="footer">
          <footer className="row">
            <p className="back-top floatright">
              <a href="index.html"><span></span></a>
            </p>
            <div className="four columns">
              <h1>ABOUT US</h1>
              Our goal is to keep clients satisfied!
            </div>
            <div className="four columns">
              <h1>GET SOCIAL</h1>
              <a className="social facebook" href="index.html">.</a> <a className="social twitter"
                href="index.html">.</a> <a className="social deviantart" href="index.html">.</a> <a
                  className="social flickr" href="index.html">.</a> <a className="social dribbble"
                    href="index.html">.</a>
            </div>
            <div className="four columns">
              <h1>NEWSLETTER</h1>
              <div className="row">
                <div className="ten columns">
                  <input type="text" placeholder="Enter your e-mail address..." />
                </div>
                <div className="two columns">
                  <a href="index.html" className="button">GO</a>
                </div>
              </div>
            </div>
          </footer>
        </div>
        <div className="copyright">
          <div className="row">
            <div className="six columns">
              &copy;<span> Copyright Your Agency Name</span>
            </div>
            <div className="six columns">
              <span className="floatright">Design by <a href="index.html">WowThemesNet</a>
              </span>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Footer;



