import React, { Component } from 'react';
import axios from 'axios';

class Testimonial extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isiQuote: ''
    };
  }

  componentDidMount() {
    axios.get('http://localhost:8080/get/1')
      .then(res => {
        this.setState({
          isiQuote : res.data
        });
      })
  }

  render() {
    return (
      <blockquote>
        <p>
          {this.state.isiQuote.quote}
          <cite>
            RICK MARTIN - NY
          </cite>
        </p>
      </blockquote>
    )
  }
}

export default Testimonial;