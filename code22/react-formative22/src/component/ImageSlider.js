import React, { Component } from 'react';

class ImageSlider extends Component {

    render() {
        return (
            <div id="ei-slider" className="ei-slider">
                <ul className="ei-slider-large">
                    <li><img src="./im/1.jpg" className="responsiveslide" alt=""/>
                        <div className ="ei-title">
                        <h2>Dare to</h2>
                        <h3>Dream</h3>
                        </div></li>
                    <li><img src="./im/2.jpg" className="responsiveslide" alt=""/>
                        <div className ="ei-title">
                        <h2>Believe</h2>
                        <h3>in Yourself</h3>
                        </div></li>
                    <li><img src="./im/3.jpg" className="responsiveslide" alt=""/>
                        <div className ="ei-title">
                        <h2>Don't</h2>
                        <h3>Give Up</h3>
                        </div></li>
                    <li><img src="./im/4.jpg" className="responsiveslide" alt=""/>
                        <div className ="ei-title">
                        <h2>Just</h2>
                        <h3>Do It!</h3>
                        </div></li>
                    <li><img src="./im/5.jpg" className="responsiveslide" alt=""/>
                        <div className ="ei-title">
                        <h2>Never</h2>
                        <h3>Say Never</h3>
                        </div></li>
                    <li><img src="./im/6.jpg" className="responsiveslide" alt=""/>
                        <div className ="ei-title">
                        <h2>Love</h2>
                        <h3>Yourself</h3>
                        </div></li>
                    <li><img src="./im/7.jpg" className="responsiveslide" alt=""/>
                        <div className ="ei-title">
                        <h2>You're</h2>
                        <h3>Your Own Hero</h3>
                        </div></li>
                    <li><img src="./im/8.jpg" className="responsiveslide" alt=""/>
                        <div className ="ei-title">
                        <h2>Catch</h2>
                        <h3>Your Dream</h3>
                        </div></li>
                    <li><img src="./im/9.jpg" className="responsiveslide" alt=""/>
                        <div className ="ei-title">
                        <h2>Make It</h2>
                        <h3>Right</h3>
                        </div></li>
                    <li><img src="./im/10.jpg" className="responsiveslide" alt=""/>
                        <div className ="ei-title">
                        <h2>You're</h2>
                        <h3>Worth It</h3>
                        </div></li>
                </ul>
                <ul className="ei-slider-thumbs">
                    <li className="ei-slider-element">Current</li>
                    <li><a href="">Slide 1</a> <img src="./im/1.jpg"
                        className="slideshowthumb" alt=""/></li>
                    <li><a href="">Slide 2</a> <img src="./im/2.jpg"
                        className="responsiveslide" className="slideshowthumb" /></li>
                    <li><a href="">Slide 3</a> <img src="./im/3.jpg"
                        className="slideshowthumb" alt=""/></li>
                    <li><a href="">Slide 4</a> <img src="./im/4.jpg"
                        className="slideshowthumb" alt=""/></li>
                    <li><a href="">Slide 5</a> <img src="./im/5.jpg"
                        className="responsiveslide" className="slideshowthumb" alt=""/></li>
                    <li><a href="">Slide 6</a> <img src="./im/6.jpg"
                        className="slideshowthumb" alt=""/></li>
                    <li><a href="">Slide 7</a> <img src="./im/7.jpg"
                        className="slideshowthumb" alt=""/></li>
                    <li><a href="">Slide 8</a> <img src="./im/8.jpg"
                        className="responsiveslide" className="slideshowthumb" alt=""/></li>
                    <li><a href="">Slide 9</a> <img src="./im/9.jpg"
                        className="slideshowthumb" alt=""/></li>
                    <li><a href="">Slide 10</a> <img src="./im/10.jpg"
                        className="slideshowthumb" alt=""/></li>
                </ul>
            </div>
        )
    }
}

export default ImageSlider;