import React, { Component } from 'react';
import axios from 'axios';

class ContentAbout extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isiQuote: ''
    };
  }

  componentDidMount() {
    axios.get('http://localhost:8080/get/1')
      .then(res => {
        this.setState({
          isiQuote: res.data
        });
      })
  }

  render() {
    return (
      <div class="eight columns">
        <div class="sectiontitle">
          <h4>Our History</h4>
        </div>
        <p>{this.state.isiQuote.boxOne}</p>
        <p>{this.state.isiQuote.boxTwo}</p>
        <div class="panel">
          <p>
            <b>{this.state.isiQuote.boxThree}</b>
          </p>
          <p class="text-right">
            <i>Ray Singer, Manager at "Nexsoft Design Task"</i>
          </p>
        </div>

        <p>{this.state.isiQuote.quote}</p>
        <dl class="tabs">
          <dd class="active">
            <a href="#simple1">Why choose us</a>
          </dd>
          <dd>
            <a href="#simple2">How we work</a>
          </dd>
          <dd>
            <a href="#simple3">Completion time</a>
          </dd>
          <dd>
            <a href="#simple4">Prices and budget</a>
          </dd>
        </dl>
        <ul class="tabs-content">
          <li class="active" id="simple1Tab">
            <p>{this.state.isiQuote.boxOne} </p>
            <p>{this.state.isiQuote.boxTwo}</p>
            <p>{this.state.isiQuote.boxThree}</p>
          </li>
          <li id="simple2Tab">{this.state.isiQuote.boxOne}</li>
          <li id="simple3Tab">{this.state.isiQuote.boxOne}</li>
          <li id="simple4Tab">{this.state.isiQuote.quote}</li>
        </ul>
      </div>
    )
  }
}

export default ContentAbout;