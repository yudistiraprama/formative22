import React from "react";
import axios from 'axios';
import { withRouter } from 'react-router-dom';

class FormInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      box1: '',
      box2: '',
      box3: '',
      box4: '',
    };
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit = event => {
    event.preventDefault();

    let data = {
      boxOne: this.state.box1,
      boxTwo: this.state.box2,
      boxThree: this.state.box3,
      quote: this.state.box4
    }
      ;

    axios.put('http://localhost:8080/addBox/1', data)
      .then((res) => {
        console.log(res)
      }).catch((err) => {
        console.log(err)
      })
    this.props.history.push('/');
    this.props.history.go(0);
  }

  render() {
    return (
      <div className="constructor" style={{
        padding: 50,
        marginLeft: 200,
        marginRight: 200,
        backgroundColor: "#ededed"
      }}>
        <h2 style={{ textAlign: "center" }}>INPUT HOME DATA</h2>
        <div>
          <form onSubmit={this.handleSubmit}>
            <div>
              <label>BOX </label>
              <textarea id="box1" name="box1" type ="text" rows="4" onChange={this.handleChange} />
            </div>
            <div>
              <label>BOX 2</label>
              <textarea id="box2" name="box2" type ="text" rows="4" onChange={this.handleChange} />
            </div>
            <div>
              <label>BOX 3</label>
              <textarea id="box3" name="box3" type ="text" rows="4" onChange={this.handleChange} />
            </div>
            <div>
              <label>QUOTES</label>
              <textarea id="box4" name="box4" type ="text" rows="4" onChange={this.handleChange} />
            </div>

            <div>
              <input type="submit" />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default withRouter(FormInput);