import React, { Component } from 'react';

class Header extends Component {

  render() {
    return (
      <div className="row">
        <div className="headerlogo four columns">
          <div className="logo">
            <a href="/index">
              <h4>Nexsoft Design Task</h4>
            </a>
          </div>
        </div>
        <div className="headermenu eight columns">
          <nav>
            <ul className="nav-bar">
              <li><a href="/" >Home</a></li>
              <li><a href="/about">About</a></li>
              <li><a href="/form">Form</a></li>
            </ul>
          </nav>
        </div>
      </div>
    )
  }
}

export default Header;

